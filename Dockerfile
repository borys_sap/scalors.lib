FROM node:15-alpine3.10

ARG Mainteiner="Nick Lototskiy"
ARG Name="Watch-And-Build"
ARG Version="0.0.1"

ENV NODE_ENV="dev"
ENV PORT=3011
ENV HOST="0.0.0.0"
ENV DB_HOST="wb-dev.c4s4s4fotuuw.eu-central-1.rds.amazonaws.com"
ENV DB_PORT=5432
ENV DB_USER="postgres"
ENV DB_PASS="very_secure_password123"
ENV DB_NAME="postgres"

LABEL MAINTAINER=${Mainteiner}
LABEL NAME=${Name}
LABEL VERSION=${Version}

WORKDIR server
COPY . .

EXPOSE ${PORT}

RUN npm install --no-cache
