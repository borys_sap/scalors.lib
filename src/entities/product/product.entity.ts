import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { ProductTranslation } from './product-translation.entity';
import {
  Chapter,
  Part,
  Subtitle,
  Tip,
  Tool,
  Video,
  VideoDocument,
} from '../video';

@Entity()
export class Product extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @Column('boolean', { default: false })
  isActive: boolean;

  @ApiProperty()
  @Column('varchar')
  productNumber: string;

  @ApiProperty()
  @Column('varchar')
  program: string;

  @ApiProperty()
  @Column('text')
  thumbnail: string;

  @ApiProperty()
  @Column('text', { nullable: true })
  companyLogo: string;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  @Expose()
  get description(): string {
    return this.translations?.[0].description;
  }

  @OneToMany(() => Chapter, (chapter) => chapter.product)
  @Transform(() => undefined, { toPlainOnly: true })
  chapters: Chapter[];

  @OneToMany(() => Part, (part) => part.product)
  @Transform(() => undefined, { toPlainOnly: true })
  parts: Part[];

  @OneToMany(() => Tip, (tip) => tip.product)
  @Transform(() => undefined, { toPlainOnly: true })
  tips: Tip[];

  @OneToMany(() => Tool, (tool) => tool.product)
  @Transform(() => undefined, { toPlainOnly: true })
  tools: Tool[];

  @ApiProperty()
  @OneToMany(() => Video, (video) => video.product)
  videos: Video[];

  @ApiProperty()
  @OneToMany(() => Subtitle, (subtitle) => subtitle.product)
  subtitles: Subtitle[];

  @OneToMany(() => VideoDocument, (videoDoc) => videoDoc.product)
  @Transform(() => undefined, { toPlainOnly: true })
  videoDocuments: VideoDocument[];

  @OneToMany(() => ProductTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: ProductTranslation[];
}
