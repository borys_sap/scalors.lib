import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';
import { Tip } from './tip.entity';

@Entity()
export class TipTranslation implements Translation<Tip> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @Column('text')
  text: string;

  @ManyToOne(() => Tip, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: Tip;
}
