import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Part } from './part.entity';
import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';

@Entity()
export class PartTranslation implements Translation<Part> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @ManyToOne(() => Part, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: Part;
}
