import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { TipCategoryTranslation } from './tip-category-translation.entity';
import { Tip } from './tip.entity';

@Entity()
export class TipCategory extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @OneToMany(() => Tip, (tip) => tip.category)
  tips: Tip[];

  @OneToMany(() => TipCategoryTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: TipCategoryTranslation[];
}
