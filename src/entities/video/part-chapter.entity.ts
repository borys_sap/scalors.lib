import { Column, Entity, ManyToOne } from 'typeorm';
import { Part } from './part.entity';
import { Chapter } from './chapter.entity';

@Entity()
export class PartChapter {
  @Column({ type: 'int', default: 0 })
  quantity: number;

  @Column({ type: 'boolean', default: false })
  is_all: boolean;

  @ManyToOne(() => Part, (part) => part.partChapters, {
    primary: true,
    onDelete: 'CASCADE',
  })
  public part: Part;

  @ManyToOne(() => Chapter, (chapter) => chapter.partChapters, {
    primary: true,
    onDelete: 'CASCADE',
  })
  public chapter!: Chapter;
}
