import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Product } from '../product';

@Entity()
@Unique(['label', 'product'])
export class Subtitle extends BaseEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('varchar')
  source: string;

  @ApiProperty()
  @Column('varchar')
  label: string;

  @ManyToOne(() => Product, (product) => product.videos)
  product: Product;
}
