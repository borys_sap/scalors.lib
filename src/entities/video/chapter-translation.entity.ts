import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Chapter } from './chapter.entity';
import { LanguageCode } from '../../config/language-code.enum';
import { Translation } from '../../base/locale-types';

@Entity()
export class ChapterTranslation implements Translation<Chapter> {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  @Index()
  languageCode: LanguageCode;

  @Column('varchar')
  name: string;

  @ManyToOne(() => Chapter, (base) => base.translations, {
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  base: Chapter;
}
