import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { BaseEntity } from '../../base/base.entity';
import { Translatable } from '../../base/locale-types';
import { Chapter } from './chapter.entity';
import { VideoDocument } from './video-document.entity';
import { ToolTranslation } from './tool-translation.entity';
import { Product } from '../product';

@Entity()
export class Tool extends BaseEntity implements Translatable {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column('text')
  thumbnail: string;

  @ApiProperty()
  @Expose()
  get name(): string {
    return this.translations?.[0].name;
  }

  @ApiProperty()
  @Expose()
  get description(): string {
    return this.translations?.[0].description;
  }

  @ManyToMany(() => Chapter, (chapter) => chapter.tools)
  @JoinTable({ name: 'tool_chapter' })
  chapters: Chapter[];

  @ManyToMany(() => VideoDocument, (videoDocument) => videoDocument.tools)
  @JoinTable({ name: 'tool_video_document' })
  videoDocuments: VideoDocument[];

  @ManyToOne(() => Product, (product) => product.tools)
  product: Product;

  @OneToMany(() => ToolTranslation, (translation) => translation.base)
  @Transform(() => undefined, { toPlainOnly: true })
  translations: ToolTranslation[];
}
