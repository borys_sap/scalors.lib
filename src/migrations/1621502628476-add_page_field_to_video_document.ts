import { MigrationInterface, QueryRunner } from 'typeorm';

export class addPageFieldToVideoDocument1621502628476
  implements MigrationInterface
{
  name = 'addPageFieldToVideoDocument1621502628476';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video_document" ADD "page" integer NOT NULL DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "video_document" DROP COLUMN "page"`);
  }
}
