import { MigrationInterface, QueryRunner } from 'typeorm';

export class createConstructionTimeTable1619786753519
  implements MigrationInterface
{
  name = 'createConstructionTimeTable1619786753519';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "construction_time_type_enum" AS ENUM('beginner', 'handyman', 'professional')`,
    );
    await queryRunner.query(
      `CREATE TABLE "construction_time" ("id" SERIAL NOT NULL, "type" "construction_time_type_enum" NOT NULL, "duration" integer NOT NULL, "chapter_id" integer, CONSTRAINT "PK_ba799d262a26fc21eca9e0e8faf" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "construction_time" ADD CONSTRAINT "FK_1ec7e790bae25afdfde319c1eb1" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "construction_time" DROP CONSTRAINT "FK_1ec7e790bae25afdfde319c1eb1"`,
    );
    await queryRunner.query(`DROP TABLE "construction_time"`);
    await queryRunner.query(`DROP TYPE "construction_time_type_enum"`);
  }
}
