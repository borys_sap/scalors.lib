import { MigrationInterface, QueryRunner } from 'typeorm';

export class isActiveProduct1627507846099 implements MigrationInterface {
  name = 'isActiveProduct1627507846099';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video_availability" DROP CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "product" ADD "is_active" boolean NOT NULL DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_availability" ADD CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d" FOREIGN KEY ("video_id") REFERENCES "video"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(`Update "product" set is_active=true`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "video_availability" DROP CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d"`,
    );
    await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "is_active"`);
    await queryRunner.query(
      `ALTER TABLE "video_availability" ADD CONSTRAINT "FK_736872089dd2d0dd4bfe36e0e5d" FOREIGN KEY ("video_id") REFERENCES "video"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
