import {MigrationInterface, QueryRunner} from "typeorm";

export class addCompanyLogo1628847807496 implements MigrationInterface {
    name = 'addCompanyLogo1628847807496'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" ADD "company_logo" text`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "company_logo"`);
    }

}
