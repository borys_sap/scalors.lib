import {MigrationInterface, QueryRunner} from "typeorm";

export class dropUselessIndexes1624444711745 implements MigrationInterface {
    name = 'dropUselessIndexes1624444711745'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_e173dc9d31d2adddb8dc73843e"`);
        await queryRunner.query(`DROP INDEX "IDX_77c5bafd1c3845ccaad9c90491"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "IDX_77c5bafd1c3845ccaad9c90491" ON "part_video_document" ("video_document_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_e173dc9d31d2adddb8dc73843e" ON "part_video_document" ("part_id") `);
    }

}
