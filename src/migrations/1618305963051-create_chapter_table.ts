import { MigrationInterface, QueryRunner } from 'typeorm';

export class createChapterTable1618305963051 implements MigrationInterface {
  name = 'createChapterTable1618305963051';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "chapter" ("id" SERIAL NOT NULL, "video_id" integer NOT NULL, "thumbnail" text NOT NULL, "start_time" integer NOT NULL, "end_time" integer NOT NULL, CONSTRAINT "PK_275bd1c62bed7dff839680614ca" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_83a5315c3498e5708786094b19" ON "chapter" ("video_id") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_83a5315c3498e5708786094b19"`);
    await queryRunner.query(`DROP TABLE "chapter"`);
  }
}
