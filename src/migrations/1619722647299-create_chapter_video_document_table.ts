import { MigrationInterface, QueryRunner } from 'typeorm';

export class createChapterVideoDocumentTable1619722647299
  implements MigrationInterface
{
  name = 'createChapterVideoDocumentTable1619722647299';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "chapter_video_document" ("chapter_id" integer NOT NULL, "video_document_id" integer NOT NULL, CONSTRAINT "PK_80c103185094f66749110c9246a" PRIMARY KEY ("chapter_id", "video_document_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_1590ed4b7ad3163fb4cd649f34" ON "chapter_video_document" ("chapter_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_8507559d491de05b2a704a5f5e" ON "chapter_video_document" ("video_document_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_video_document" ADD CONSTRAINT "FK_1590ed4b7ad3163fb4cd649f348" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_video_document" ADD CONSTRAINT "FK_8507559d491de05b2a704a5f5ed" FOREIGN KEY ("video_document_id") REFERENCES "video_document"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "chapter_video_document" DROP CONSTRAINT "FK_8507559d491de05b2a704a5f5ed"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter_video_document" DROP CONSTRAINT "FK_1590ed4b7ad3163fb4cd649f348"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_8507559d491de05b2a704a5f5e"`);
    await queryRunner.query(`DROP INDEX "IDX_1590ed4b7ad3163fb4cd649f34"`);
    await queryRunner.query(`DROP TABLE "chapter_video_document"`);
  }
}
