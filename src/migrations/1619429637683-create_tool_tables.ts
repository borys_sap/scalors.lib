import { MigrationInterface, QueryRunner } from 'typeorm';

export class createToolTables1619429637683 implements MigrationInterface {
  name = 'createToolTables1619429637683';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "tool_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "description" text NOT NULL, "base_id" integer, CONSTRAINT "PK_6952e1d015a4256afbcfa6a39ec" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_2bace3b2afc6c42f0ebda2ff64" ON "tool_translation" ("language_code") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tool" ("id" SERIAL NOT NULL, "thumbnail" text NOT NULL, CONSTRAINT "PK_3bf5b1016a384916073184f99b7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "tool_chapter" ("tool_id" integer NOT NULL, "chapter_id" integer NOT NULL, CONSTRAINT "PK_7ed2492279d4c03bfebbae31280" PRIMARY KEY ("tool_id", "chapter_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e931b208803c34f4ca4b22664d" ON "tool_chapter" ("tool_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3827b0468335c0929120180423" ON "tool_chapter" ("chapter_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "tool_video_document" ("tool_id" integer NOT NULL, "video_document_id" integer NOT NULL, CONSTRAINT "PK_e7319ef0f58aac9ea81d455a9a0" PRIMARY KEY ("tool_id", "video_document_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_b150d2281cf609e536da23f72a" ON "tool_video_document" ("tool_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_20498c0dc19703149d01ada698" ON "tool_video_document" ("video_document_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_translation" ADD CONSTRAINT "FK_301358e0be128210752f49ccd20" FOREIGN KEY ("base_id") REFERENCES "tool"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_chapter" ADD CONSTRAINT "FK_e931b208803c34f4ca4b22664d0" FOREIGN KEY ("tool_id") REFERENCES "tool"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_chapter" ADD CONSTRAINT "FK_3827b0468335c09291201804232" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_video_document" ADD CONSTRAINT "FK_b150d2281cf609e536da23f72ac" FOREIGN KEY ("tool_id") REFERENCES "tool"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_video_document" ADD CONSTRAINT "FK_20498c0dc19703149d01ada6989" FOREIGN KEY ("video_document_id") REFERENCES "video_document"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "tool_video_document" DROP CONSTRAINT "FK_20498c0dc19703149d01ada6989"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_video_document" DROP CONSTRAINT "FK_b150d2281cf609e536da23f72ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_chapter" DROP CONSTRAINT "FK_3827b0468335c09291201804232"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_chapter" DROP CONSTRAINT "FK_e931b208803c34f4ca4b22664d0"`,
    );
    await queryRunner.query(
      `ALTER TABLE "tool_translation" DROP CONSTRAINT "FK_301358e0be128210752f49ccd20"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_20498c0dc19703149d01ada698"`);
    await queryRunner.query(`DROP INDEX "IDX_b150d2281cf609e536da23f72a"`);
    await queryRunner.query(`DROP TABLE "tool_video_document"`);
    await queryRunner.query(`DROP INDEX "IDX_3827b0468335c0929120180423"`);
    await queryRunner.query(`DROP INDEX "IDX_e931b208803c34f4ca4b22664d"`);
    await queryRunner.query(`DROP TABLE "tool_chapter"`);
    await queryRunner.query(`DROP TABLE "tool"`);
    await queryRunner.query(`DROP INDEX "IDX_2bace3b2afc6c42f0ebda2ff64"`);
    await queryRunner.query(`DROP TABLE "tool_translation"`);
  }
}
