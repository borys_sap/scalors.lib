import {MigrationInterface, QueryRunner} from "typeorm";

export class addPartDocQuantityField1624351206294 implements MigrationInterface {
    name = 'addPartDocQuantityField1624351206294'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_e173dc9d31d2adddb8dc73843e"`);
        await queryRunner.query(`DROP INDEX "IDX_77c5bafd1c3845ccaad9c90491"`);
        await queryRunner.query(`ALTER TABLE "part_video_document" ADD "quantity" integer NOT NULL DEFAULT '0'`);
        await queryRunner.query(`CREATE INDEX "IDX_e173dc9d31d2adddb8dc73843e" ON "part_video_document" ("part_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_77c5bafd1c3845ccaad9c90491" ON "part_video_document" ("video_document_id") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_77c5bafd1c3845ccaad9c90491"`);
        await queryRunner.query(`DROP INDEX "IDX_e173dc9d31d2adddb8dc73843e"`);
        await queryRunner.query(`ALTER TABLE "part_video_document" DROP COLUMN "quantity"`);
        await queryRunner.query(`CREATE INDEX "IDX_77c5bafd1c3845ccaad9c90491" ON "part_video_document" ("video_document_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_e173dc9d31d2adddb8dc73843e" ON "part_video_document" ("part_id") `);
    }

}
