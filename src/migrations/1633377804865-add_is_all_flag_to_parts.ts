import {MigrationInterface, QueryRunner} from "typeorm";

export class addIsAllFlagToParts1633377804865 implements MigrationInterface {
    name = 'addIsAllFlagToParts1633377804865'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "part_chapter" ADD "is_all" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "part_video_document" ADD "is_all" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "part_video_document" DROP COLUMN "is_all"`);
        await queryRunner.query(`ALTER TABLE "part_chapter" DROP COLUMN "is_all"`);
    }

}
