import { MigrationInterface, QueryRunner } from 'typeorm';

export class addFieldsToChapterTable1619678989117
  implements MigrationInterface
{
  name = 'addFieldsToChapterTable1619678989117';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "chapter" ADD "number_of_people" integer NOT NULL DEFAULT 0`,
    );
    await queryRunner.query(
      `CREATE TYPE "chapter_difficulty_level_enum" AS ENUM('low', 'middle', 'high')`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" ADD "difficulty_level" "chapter_difficulty_level_enum" NOT NULL DEFAULT 'middle'`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" ADD "order_dependency" boolean NOT NULL DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "chapter" DROP COLUMN "order_dependency"`,
    );
    await queryRunner.query(
      `ALTER TABLE "chapter" DROP COLUMN "difficulty_level"`,
    );
    await queryRunner.query(`DROP TYPE "chapter_difficulty_level_enum"`);
    await queryRunner.query(
      `ALTER TABLE "chapter" DROP COLUMN "number_of_people"`,
    );
  }
}
