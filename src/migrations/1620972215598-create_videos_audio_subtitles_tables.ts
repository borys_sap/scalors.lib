import { MigrationInterface, QueryRunner } from 'typeorm';

export class createVideosAudioSubtitlesTables1620972215598
  implements MigrationInterface
{
  name = 'createVideosAudioSubtitlesTables1620972215598';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "video_source" ("id" SERIAL NOT NULL, "bitrate" integer NOT NULL, "source" character varying NOT NULL, "language" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_afcc77c2250684b671150de4ba5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "subtitle" ("id" SERIAL NOT NULL, "source" character varying NOT NULL, "language" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_994ad1599c74d6da447883869b5" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "video" ("id" SERIAL NOT NULL, "longevity" numeric NOT NULL, "preview" text NOT NULL, "creation" integer NOT NULL, CONSTRAINT "PK_1a2f3856250765d72e7e1636c8e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "audio" ("id" SERIAL NOT NULL, "source" character varying NOT NULL, "language" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_9562215b41192ae4ccdf314a789" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_source" ADD CONSTRAINT "FK_de1fefc7c501d8f974233366045" FOREIGN KEY ("base_id") REFERENCES "video"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "subtitle" ADD CONSTRAINT "FK_63acc9c41cd3bf5e7ed63d0bd9f" FOREIGN KEY ("base_id") REFERENCES "video"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "audio" ADD CONSTRAINT "FK_6e4d104e5576b3578a76122c55e" FOREIGN KEY ("base_id") REFERENCES "video"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "audio" DROP CONSTRAINT "FK_6e4d104e5576b3578a76122c55e"`,
    );
    await queryRunner.query(
      `ALTER TABLE "subtitle" DROP CONSTRAINT "FK_63acc9c41cd3bf5e7ed63d0bd9f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "video_source" DROP CONSTRAINT "FK_de1fefc7c501d8f974233366045"`,
    );
    await queryRunner.query(`DROP TABLE "audio"`);
    await queryRunner.query(`DROP TABLE "video"`);
    await queryRunner.query(`DROP TABLE "subtitle"`);
    await queryRunner.query(`DROP TABLE "video_source"`);
  }
}
