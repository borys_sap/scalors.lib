import { MigrationInterface, QueryRunner } from 'typeorm';

export class addDimensionsFieldToPart1621516395368
  implements MigrationInterface
{
  name = 'addDimensionsFieldToPart1621516395368';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "part" ADD "dimensions" character varying(32) NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "construction_time" DROP CONSTRAINT "FK_1ec7e790bae25afdfde319c1eb1"`,
    );
  }
}
