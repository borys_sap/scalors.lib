import { MigrationInterface, QueryRunner } from 'typeorm';

export class createPartTables1619071690488 implements MigrationInterface {
  name = 'createPartTables1619071690488';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "part_translation" ("id" SERIAL NOT NULL, "language_code" character varying NOT NULL, "name" character varying NOT NULL, "base_id" integer, CONSTRAINT "PK_2c8ca4c7f8b25a31d3aefc0a1fb" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_c3d96aeff637f7677f0ce8f6cd" ON "part_translation" ("language_code") `,
    );
    await queryRunner.query(
      `CREATE TYPE "part_size_enum" AS ENUM('small', 'big')`,
    );
    await queryRunner.query(
      `CREATE TABLE "part" ("id" SERIAL NOT NULL, "thumbnail" text NOT NULL, "part_number" character varying(32) NOT NULL, "quantity" integer NOT NULL, "size" "part_size_enum" NOT NULL, CONSTRAINT "PK_58888debdf048d2dfe459aa59da" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "part_chapter" ("part_id" integer NOT NULL, "chapter_id" integer NOT NULL, CONSTRAINT "PK_d670326e0192e2d8c1fa9539597" PRIMARY KEY ("part_id", "chapter_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_41bca8758e31937a6582fa0d77" ON "part_chapter" ("part_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_44fa764c296ca40246e77ea3a8" ON "part_chapter" ("chapter_id") `,
    );
    await queryRunner.query(
      `CREATE TABLE "part_video_document" ("part_id" integer NOT NULL, "video_document_id" integer NOT NULL, CONSTRAINT "PK_7ed4ab3830388ca7247193b70cb" PRIMARY KEY ("part_id", "video_document_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_e173dc9d31d2adddb8dc73843e" ON "part_video_document" ("part_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_77c5bafd1c3845ccaad9c90491" ON "part_video_document" ("video_document_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "part_translation" ADD CONSTRAINT "FK_fbad730c9202486183e46a43a96" FOREIGN KEY ("base_id") REFERENCES "part"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_chapter" ADD CONSTRAINT "FK_41bca8758e31937a6582fa0d777" FOREIGN KEY ("part_id") REFERENCES "part"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_chapter" ADD CONSTRAINT "FK_44fa764c296ca40246e77ea3a85" FOREIGN KEY ("chapter_id") REFERENCES "chapter"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_video_document" ADD CONSTRAINT "FK_e173dc9d31d2adddb8dc73843e3" FOREIGN KEY ("part_id") REFERENCES "part"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_video_document" ADD CONSTRAINT "FK_77c5bafd1c3845ccaad9c904917" FOREIGN KEY ("video_document_id") REFERENCES "video_document"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "part_video_document" DROP CONSTRAINT "FK_77c5bafd1c3845ccaad9c904917"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_video_document" DROP CONSTRAINT "FK_e173dc9d31d2adddb8dc73843e3"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_chapter" DROP CONSTRAINT "FK_44fa764c296ca40246e77ea3a85"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_chapter" DROP CONSTRAINT "FK_41bca8758e31937a6582fa0d777"`,
    );
    await queryRunner.query(
      `ALTER TABLE "part_translation" DROP CONSTRAINT "FK_fbad730c9202486183e46a43a96"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_77c5bafd1c3845ccaad9c90491"`);
    await queryRunner.query(`DROP INDEX "IDX_e173dc9d31d2adddb8dc73843e"`);
    await queryRunner.query(`DROP TABLE "part_video_document"`);
    await queryRunner.query(`DROP INDEX "IDX_44fa764c296ca40246e77ea3a8"`);
    await queryRunner.query(`DROP INDEX "IDX_41bca8758e31937a6582fa0d77"`);
    await queryRunner.query(`DROP TABLE "part_chapter"`);
    await queryRunner.query(`DROP TABLE "part"`);
    await queryRunner.query(`DROP TYPE "part_size_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_c3d96aeff637f7677f0ce8f6cd"`);
    await queryRunner.query(`DROP TABLE "part_translation"`);
  }
}
